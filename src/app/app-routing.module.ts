import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InicioComponent } from './pages/inicio/inicio.component';
import { ProductosComponent } from './pages/productos/productos.component';
import { ContactoComponent } from './pages/contacto/contacto.component';
import { FabricantesComponent } from './pages/fabricantes/fabricantes.component';
import { AboutComponent } from './pages/about/about.component';
import { PromocionesComponent } from './pages/promociones/promociones.component';


const routes: Routes = [
  { path: 'inicio', component: InicioComponent },
  { path: 'productos', component: ProductosComponent},
  { path: 'contacto', component: ContactoComponent},
  { path: 'fabricantes', component: FabricantesComponent},
  { path: 'about', component: AboutComponent},
  { path: 'promociones', component: PromocionesComponent},
  { path: '**', pathMatch: 'full', redirectTo: 'inicio'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
